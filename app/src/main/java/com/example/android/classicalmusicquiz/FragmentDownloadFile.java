package com.example.android.classicalmusicquiz;

/**
 * Created by kloneworld on 03/10/17.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class FragmentDownloadFile extends Fragment
         {

    private static final String PERSISTENT_VARIABLE_BUNDLE_KEY = "persistentVariableBackPressed";
    private static final String PERSISTENT_VARIABLE_BUNDLE_KEY1 = "persistentVariableImageURL";
    private static final String TAG = "MyActivity";

    Bitmap bitmap = null; String videoPath ="";

    // button to show progress dialog
    RelativeLayout downloadVideo; FrameLayout thumbnail;
    // Progress Dialog
    private ProgressDialog pDialog;
    ImageView my_image, button;
    ImageView play_button;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    public FragmentDownloadFile() {
        setArguments(new Bundle());
    }

    View view;
    // File url to download
    //private static String VideoURL = "http://autogram-server.appspot.com/serve?key=AMIfv96s90p0qoJOCRuJv6o6GGyraXsmSBKa-i-Joayn0-Hx-Nb-7TDdiax8guxRTA4j9VWkpxmkJc40xPTSmEJwKasW4kOysROcvGfjOij70VQ5YrOIQi-7KtaSRu38QxbaAMubnp0jYRO_aU8DsZFc6GDwu9H_RnnRbULM37sybX15fGlaATo&ktype=video";

    private static String VideoURL;
    //public String VideoURL= NetworkCall.execute(RequestURL);

    //JSONObject jsonObject;
    String jsonObject; TextView tap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, null);
        downloadVideo = (RelativeLayout) view.findViewById(R.id.tap_to_download);
        thumbnail =(FrameLayout) view.findViewById(R.id.thumbnail);

        // show progress bar button
        button= (ImageView) view.findViewById(R.id.button);
        // Image view to show image after downloading
        my_image = (ImageView) view.findViewById(R.id.image);
        play_button = (ImageView) view.findViewById(R.id.play);
        tap = (TextView) view.findViewById(R.id.textView);
        //Save view state
        Bundle mySavedInstanceState =  getArguments();

        //int persistentVariabletap = mySavedInstanceState.getInt(PERSISTENT_VARIABLE_BUNDLE_KEY1);
        int persistentVariable = mySavedInstanceState.getInt(PERSISTENT_VARIABLE_BUNDLE_KEY);
        //VideoURL = NetworkCall.fetchData(RequestURL).getVideoURL() ;
        /*Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    Log.v("URL", RequestURL);
                    jsonObject = NetworkCall.getJSONObjectFromURL(RequestURL);
                    Log.e("JSON", jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();*/

        File file = new File("/sdcard/downloadedfile.mp4" );
        if (file.exists()) {
            downloadVideo.setVisibility(View.INVISIBLE);
            thumbnail.setVisibility(View.VISIBLE);
             String existingVideo = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.mp4";
            my_image.setImageBitmap(createVideoThumbNail(existingVideo));
            getArguments().putString(PERSISTENT_VARIABLE_BUNDLE_KEY1, existingVideo);

            if (persistentVariable == 1) {
                my_image.setImageBitmap(createVideoThumbNail(mySavedInstanceState.getString(PERSISTENT_VARIABLE_BUNDLE_KEY1)));
            }
        }
        else
        {
            downloadVideo.setVisibility(View.VISIBLE);
            thumbnail.setVisibility(View.INVISIBLE);
        }
        /**
         * Show Progress bar click event
         * */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DownloadFileFromURL().execute(VideoURL);
            }
        });
        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                FragmentPlayVideo f2 = new FragmentPlayVideo();
                fragmentTransaction.replace(R.id.container, f2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return view;
    }
    @Override
    public void onPause() {
        super.onPause();
        getArguments().putInt(PERSISTENT_VARIABLE_BUNDLE_KEY, 1);
        getArguments().putString(PERSISTENT_VARIABLE_BUNDLE_KEY1, videoPath);
        //getArguments().putInt(PERSISTENT_VARIABLE_BUNDLE_KEY1, tapState);
    }
   /* @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                new DownloadFileFromURL().execute(VideoURL);
                break;
            case R.id.image:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentPlayVideo());
                break;
            default:
                communicator.clicked();
                break;
        }
    }
*/
    /**
     * Showing Dialog
     *
*/
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }

    }


    public interface Communicator {
        public void clicked();
        public void retrieved(String VideoURL);


    }
    private Communicator communicator;
    public void setCommunicator(Communicator communicator) {
        this.communicator = communicator;
    }

    public void changeView(){
        tap.setText("No videos yet");
    }

    public void getVideo(String Video)
             {
                 VideoURL = Video;
             }



    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        private ProgressFragment mProgressFragment;
        private static final String PROGRESS_DIALOG_TAG = "ProcessProgressDialog";
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */


        public void showProgress()
        {
            if (mProgressFragment == null)
            {
                mProgressFragment =  new ProgressFragment(getActivity(), "Downloading...", null);
            }
            mProgressFragment.setCancelable(false);
            mProgressFragment.show(getActivity().getFragmentManager(),  PROGRESS_DIALOG_TAG);
        }

        public void dismissProgress()
        {
            if (mProgressFragment != null)
            {
                if (mProgressFragment.isAdded())
                {
                    mProgressFragment.dismissAllowingStateLoss();
                }
                mProgressFragment = null;
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            File file = new File("/sdcard/downloadedfile.mp4" );
            if (file.exists()) {
                //Toast.makeText(getActivity(),"File Already Exists!",Toast.LENGTH_SHORT).show();
                Log.v(PROGRESS_DIALOG_TAG, "Dont Download");
            }
            else{
                try {
                    URL url = new URL(f_url[0]);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    //show a typical 0-100% progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    // Output stream
                    OutputStream output = new FileOutputStream("/sdcard/downloadedfile.mp4");

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
            mProgressFragment.setProgress("", progress[0], "");
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissProgress();

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            //String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            videoPath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.mp4";

            try {
                //bitmap = retriveVideoFrameFromVideo(videoPath);
                bitmap = createVideoThumbNail(videoPath);
                //Bitmap play = BitmapFactory.decodeResource(getResources(), R.drawable.play);
                //Bitmap mergedImage = createSingleImageFromMultipleImages(bitmap, play);
                my_image.setImageBitmap(bitmap);
                // setting downloaded into image view
                downloadVideo.setVisibility(View.INVISIBLE);
                thumbnail.setVisibility(View.VISIBLE);

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            //my_image.setVideoPath(videoPath);
            //my_image.setImageDrawable(Drawable.createFromPath(imagePath));

                //Drawable d = new BitmapDrawable(getResources(), bitmap);
                //my_image.setImageDrawable(d);

            //my_image.setVideoURI(Uri.parse(VideoURL));
        }

    }

    /*public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }*/
    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
    }
    /*private Bitmap createSingleImageFromMultipleImages(Bitmap firstImage, Bitmap secondImage){

        Bitmap result = Bitmap.createBitmap(firstImage.getWidth(), firstImage.getHeight(), firstImage.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(firstImage, 0f, 0f, null);
        canvas.drawBitmap(secondImage, 10, 10, null);
        return result;
    }*/




}
