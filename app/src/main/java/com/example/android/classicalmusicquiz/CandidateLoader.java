package com.example.android.classicalmusicquiz;

import android.content.AsyncTaskLoader;
import android.content.Context;

/**
 * Created by kloneworld on 06/10/17.
 */

public class CandidateLoader extends AsyncTaskLoader<Candidate> {

    private static final String LOG_TAG = CandidateLoader.class.getName();
    private String URL;
    Candidate candidate;
    public CandidateLoader(Context context, String url ){
        super(context);

        URL = url;
    }
    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    /**
     * This is on a background thread.
     */
    @Override
    public Candidate loadInBackground() {
        if (URL == null) {
            return null;
        }

        // Perform the network request, parse the response, and extract a list of earthquakes.
        //List<Earthquake> earthquakes = QueryUtils.fetchEarthquakeData(mUrl);
        candidate = NetworkCall.fetchData(URL);
        return candidate;
    }
}


