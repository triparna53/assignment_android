/*
* Copyright (C) 2017 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*  	http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.android.classicalmusicquiz;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements FragmentDownloadFile.Communicator{

    private static String token="3dpJk%2Fa5uwgCnpKZcDT9oHDQoG0eeooa1SD4QMo2z%2B5qRUaQKD6nE3Fp70hfBmHW9DdkpVAMk7paCqwuqjVF4cXRxYYFBqXF61hUnUislUyri1Hb5noz5eaqlIf8Vd0pesfv%2F%2Fbzn%2FnTtDzIhF7JX1JDyV5rui1m%2BK1LRgu%2FXH6xsIcw72UIU%2BRE%2BnnWOHSW";
    public static final String RequestURL = Constants.BaseURL+ Constants.GetDetails+ "?"+ Constants.Token+ "="+ token;

    String VideoURL;

    FragmentDownloadFile f1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentDownloadFile f1 = new FragmentDownloadFile();
        f1.setCommunicator(this);
        // Get a reference to the ConnectivityManager to check state of network connectivity
        //ConnectivityManager connMgr = (ConnectivityManager)
               // getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get details on the currently active default data network
        //NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // If there is a network connection, fetch data
        //if (networkInfo != null && networkInfo.isConnected()) {
            // Get a reference to the LoaderManager, in order to interact with loaders.
            LoaderManager loaderManager = getLoaderManager();
            //VideoURL = CandidateLoader.execute(RequestURL);
            // Initialize the loader. Pass in the int ID constant defined above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
            // because this activity implements the LoaderCallbacks interface).
            loaderManager.initLoader(1, null, this);



        getSupportFragmentManager().beginTransaction().add(R.id.container, f1).commit();




    }
   /* public static final int progress_bar_type = 0;
    private ProgressDialog pDialog;

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }

    }

*/

    /**
     * The OnClick method for the New Game button that starts a new game.
     * @param view The New Game button.

    public void newGame(View view) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentPlayVideo());

    }
*/
    ProgressFragment f2 ;
    @Override
    public void clicked() {

            f1.changeView();
        //f2.setProgress("total","value", "units");
    }
    public void retrieved(String VideoURL){
        f1.getVideo(VideoURL);
    }



    @Override
    public void onLoadFinished(android.content.Loader<Candidate> loader, Candidate candidate) {
        if(candidate!= null){
            //VideoURL = NetworkCall.fetchData(RequestURL).getVideoURL();
            VideoURL = CandidateLoader.
            retrieved(VideoURL);
        }
        else
            clicked();

    }

    @Override
    public void onLoaderReset(android.content.Loader<Candidate> loader) {
        clicked();

    }
}
